# Exercise 1: Revise a previous program as follows: Read and parse the
# “From” lines and pull out the addresses from the line. Count the number 
# of messages from each person using a dictionary.

# After all the data has been read, print the person with the most commits
# by creating a list of (count, email) tuples from the dictionary. Then
# sort the list in reverse order and print out the person who has the most
# commits.

# Sample Line:
# From stephen.marquard@uct.ac.za Sat Jan 5 09:14:16 2008

# Enter a file name: mbox-short.txt
# cwen@iupui.edu 5

# Enter a file name: mbox.txt
# zqian@umich.edu 195

# chapter 9 exercise 3 copied over and modified.
filename = input("Enter a file name: ")

if len(filename) < 2:
    filename = "mbox-short.txt"

fhand = open(filename, 'r')
email_adresses = {}

for line in fhand:
    if line.startswith("From "):
        email = line.split()[1]
        email_adresses[email] = email_adresses.get(email, 0) + 1 

lst = []
for key, value in email_adresses.items(): # converts dictionary into a list of key values
    lst.append((value,key)) # to get values first

lst.sort(reverse=True) # to get the largest number first in the list
person = lst[0] # [0] since the first element is the biggest now
print(person[1], person[0]) # since the value is first i print person[1]
# to get the email element written first instead of the value when printing.
