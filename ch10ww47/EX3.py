# Exercise 3: Write a program that reads a file and prints the letters
# in decreasing order of frequency. Your program should convert all the
# input to lower case and only count the letters a-z. Your program should
# not count spaces, digits, punctuation, or anything other than the letters
# a-z. Find text samples from several different languages and see how
# letter frequency varies between languages. Compare your results with
# the tables at https://wikipedia.org/wiki/Letter_frequencies.

letters = 'abcdefghijklmnopqrstuvxyz'

filename = input("Enter a file name: ")
if len(filename) < 2:
    filename = "mbox-short.txt"

fhand = open(filename, 'r')

text = fhand.read()
count_dict = {}
for char in text.lower():
    if char in letters:
        count_dict[char] = count_dict.get(char, 0) + 1

lst = []
for key, value in count_dict.items(): # converts dictionary into a list of key values
    lst.append((value,key)) # to get values first

lst.sort(reverse=True) # to get the largest number first in the list

for count_dict, letter in lst:
    print(letter, count_dict) # prints letter 1st
    # and the counted amount of the letter 2nd
