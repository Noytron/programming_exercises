# Exercise 2: This program counts the distribution of the hour of the day
# for each of the messages. You can pull the hour from the “From” line
# by finding the time string and then splitting that string into parts using
# the colon character. Once you have accumulated the counts for each
# hour, print out the counts, one per line, sorted by hour as shown below.

# python timeofday.py
# Enter a file name: mbox-short.txt
# 04 3
# 06 1
# 07 1
# 09 2
# 10 3
# 11 6
# 14 1
# 15 2
# 16 4
# 17 2
# 18 1
# 19 1

filename = input("Enter a file name: ")
if len(filename) < 2:   # love this 
    filename = "mbox-short.txt" # reads this file if the file entered is less than 2

fhand = open(filename, 'r')

hours_in_day = {} # empty dictionary

for line in fhand:
    if line.startswith("From "):
        time = line.split()[5]
        hour = time.split(":")[0]
        hours_in_day[hour] = hours_in_day.get(hour, 0) + 1 
        # checks the empty dictionary for the value in hour which is 0
        # then increase by 1 to count the number of times a specific hour is found.
        # so it should count how many times the hour 15 is found when i search
        # through mbox-short.txt or mbox.txt
    #    email_adresses[email] = email_adresses.get(email, 0) + 1 

lst = list(hours_in_day.items()) 

lst.sort()
for t in lst:
    print(t[0], t[1]) # prints hour first and number of times that hour is mentioned.
