# Make a line chart of the data from PY4E ch9_ex2 or ch9_ex5 
# following video1 https://youtu.be/UO98lJQ3QGI 
# just skip the install part if you are on pycharm (start at 2:00)
# Important! matplotlib uses seperate lists for the x and y axis.
# ch9_ex2 and ch9_ex5 will give you a dictionary that you have to 
# convert into one list containing the keys and another list containing the values.

from matplotlib import pyplot as plt # unable to import matplotlib and no name 'pyplot'

filename = input("Enter a file name: ") # any name of a file i want to open
if len(filename) < 2:
    filename = "mbox-short.txt"

try:
    fhand = open(filename, 'r') # read whatever file i opened
    domains = {}

    for line in fhand:
        if line.startswith("From "):
            email = line.split()[1]
            domain = email.split("@")[1]
            domains[domain] = domains.get(domain, 0) + 1

    print(domains)

    sort_list = list()

    for sender, count in list(domains.items()):
        sort_list.append((count, sender))
    sort_list.sort()

    sender_list = list()
    count_list = list()

    for count, sender in sort_list:
        sender_list.append(sender)
        count_list.append(count)

    plt.ylabel("Sender")
    plt.xlabel("Mails")

    plt.show()

except FileNotFoundError:
    print("File not found")

# currently not actually giving a filled out chart. Sender / count on x and y works.