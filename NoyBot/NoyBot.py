# bot.py
# imports
import discord
import time
import asyncio
# import pandas as pd (used for CSV files with pandas) 
intents = discord.Intents.default() # required to use 'member' commands
intents.typing = True    # on by default
intents.presences = True # not on by default
intents.members = True   # not on by default

from discord.ext import commands

#client = discord.Client() old method
client = commands.Bot(command_prefix='!', intents=intents)

@client.command(name='rules')
async def version(context):
    print('here')
    myEmbed = discord.Embed(title = "The Rules", description ="Breaking any of the following rules will result in a Metsuke jailing, kicking, or banning you. It is your own responsibility to carefully read these rules and avoid breaking them. If you choose not to read, and then break the rules it is no one's fault but your own, and you will have to deal with the consequences." , color = 0x00FFA500)
    myEmbed.add_field(name = "1: ", value = "No racism/discrimination of any kind.", inline=False)
    myEmbed.add_field(name = "2: ", value = "No politics/religion talk of any kind.", inline=False)
    myEmbed.add_field(name = "3: ", value = "No spamming, by which I mean copy-pasting malicious links, images, or otherwise disturbing content. Posting several messages after one another is not a problem as long as you're not doing it to annoy people.", inline=False)
    myEmbed.add_field(name = "4: ", value = "Show respect to others, if you want to name-call each other, do so in private chat.", inline=False)
    myEmbed.add_field(name = "5: ", value = "Do not only advertise your own content, you are free to link your own content in the appropriate chats as long as you also participate in others.", inline=False)
    myEmbed.add_field(name = "6: ", value = "Stick to the proper channels, e.g. keep videos in #📺videos-links and images in #🖼images, #🗑memes, and #🥔middle-earth-memes chats. All Total War related content must go in #⚔total-war, all Paradox Games content in #🛡paradox-games, all Mount & Blade related content in #🐎mount-and-blade, etc. This rule is for levies only.", inline=False)
    myEmbed.add_field(name = "7: ", value = "Stick to English in the text channels. If you're playing a game with your friend using voice channels feel free to speak whatever language you desire.", inline=False)
    myEmbed.set_footer(text="My Metsukes and I reserve the right to jail, kick, or ban anyone even if they haven't broken any rules necessarily if we feel they are a negative influence on the server. This isn't something that is done lightly.")
    myEmbed.set_author(name="MrSmartDonkey") 

    await context.message.channel.send(embed=myEmbed) # awaits the use of the embed

@client.event
async def on_ready(): # happens whenever the bot goes online
    welcome_hall = client.get_channel(778559816951267343)
    await welcome_hall.send('My spear is ready to serve!')

    await client.change_presence(status=discord.Status.do_not_disturb, activity=discord.Game('Total War: Three Kingdoms'))
    await asyncio.sleep(60)
    await client.change_presence(status=discord.Status.do_not_disturb, activity=discord.Game('A Total War Saga: Troy'))
    await asyncio.sleep(60)
    await client.change_presence(status=discord.Status.do_not_disturb, activity=discord.Game('Total War: Elysium'))
    
    # future plans: making it loop.
    # future plans: making it default to any channel

@client.event
async def on_message(message):
    if message.content.find("!Hi") != -1:
     #   await member.message.channel.send(" ") != -1:
        await message.channel.send("Hi")

    elif message.content == ("what are the rules?"):
        welcome_hall = client.get_channel(778559816951267343)

        myEmbed = discord.Embed(title = "The Rules", description ="Breaking any of the following rules will result in a Metsuke jailing, kicking, or banning you. It is your own responsibility to carefully read these rules and avoid breaking them. If you choose not to read, and then break the rules it is no one's fault but your own, and you will have to deal with the consequences." , color = 0x00FFA500)
        myEmbed.add_field(name = "1: ", value = "No racism/discrimination of any kind.", inline=False)
        myEmbed.add_field(name = "2: ", value = "No politics/religion talk of any kind.", inline=False)
        myEmbed.add_field(name = "3: ", value = "No spamming, by which I mean copy-pasting malicious links, images, or otherwise disturbing content. Posting several messages after one another is not a problem as long as you're not doing it to annoy people.", inline=False)
        myEmbed.add_field(name = "4: ", value = "Show respect to others, if you want to name-call each other, do so in private chat.", inline=False)
        myEmbed.add_field(name = "5: ", value = "Do not only advertise your own content, you are free to link your own content in the appropriate chats as long as you also participate in others.", inline=False)
        myEmbed.add_field(name = "6: ", value = "Stick to the proper channels, e.g. keep videos in #📺videos-links and images in #🖼images, #🗑memes, and #🥔middle-earth-memes chats. All Total War related content must go in #⚔total-war, all Paradox Games content in #🛡paradox-games, all Mount & Blade related content in #🐎mount-and-blade, etc. This rule is for levies only.", inline=False)
        myEmbed.add_field(name = "7: ", value = "Stick to English in the text channels. If you're playing a game with your friend using voice channels feel free to speak whatever language you desire.", inline=False)
        myEmbed.set_footer(text="My Metsukes and I reserve the right to jail, kick, or ban anyone even if they haven't broken any rules necessarily if we feel they are a negative influence on the server. This isn't something that is done lightly.")
        myEmbed.set_author(name="MrSmartDonkey")
        
        await welcome_hall.send(embed=myEmbed)
    await client.process_commands(message)

@client.event 
async def on_member_join(member):
    await member.create_dm()
    await member.dm_channel.send(f'Hi {member.name}, welcome to my discord server!')


       # await welcome_hall.send('Yarimazing! welcome levy, to our great dojo!')

client.run('Nzc4MTE3MTEzMTMwNjQ3NTUy.X7NUCA.RqHhWdT5zBFgxAMDGReT7kfStXs') # don't ever share token