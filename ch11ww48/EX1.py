# Exercise 1: Write a simple program to simulate the operation of the
# grep command on Unix. Ask the user to enter a regular expression and
# count the number of lines that matched the regular expression:

# $ python grep.py
# Enter a regular expression: ^Author
# mbox.txt had 1798 lines that matched ^Author

# $ python grep.py
# Enter a regular expression: ^X-
# mbox.txt had 14368 lines that matched ^X-

# $ python grep.py
# Enter a regular expression: java$
# mbox.txt had 4175 lines that matched java$

import re

filename = input("enter the filename: ")
if len(filename) < 2:
    filename = "mbox.txt"
# regex (regular expression)
regex = input("Enter a regular expression: ")
if len(regex) < 2:
    regex = "java$"

counter = 0
fhand = open(filename, 'r')
for line in fhand:
    line = line.rstrip() # makes the line always end with java instead of a white space.
    if re.search(regex, line):
        counter += 1

print(counter)

# ^Author = 1798
# ^X- = 14368
# java$ = 4218 ???