# Exercise 2: Write a program to look for lines of the form:
# New Revision: 39772

# Extract the number from each of the lines using a regular expression
# and the findall() method. Compute the average of the numbers and
# print out the average as an integer.

# Enter file:mbox.txt
# 38549

# Enter file:mbox-short.txt
# 39756

import re

hand = open('mbox-short.txt')
for line in hand:
    line = line.rstrip()
    match = re.findall('^New\\sRevision\\S*: ([0-9.]+)', line)
    if len(match) > 0:

        print(match)
        # this way i print the numbers using findall however i can't figure out how to use
        # 'sum' with findall so i made a setup using re.search instead which gets the average.