# Exercise 2: Write a program to look for lines of the form:
# New Revision: 39772

# Extract the number from each of the lines using a regular expression
# and the findall() method. Compute the average of the numbers and
# print out the average as an integer.

# Enter file:mbox.txt
# 38549

# Enter file:mbox-short.txt
# 39756

import re

filename = input("Enter filename: ")
if len(filename) < 2:
    filename = "mbox-short.txt"

fhand = open(filename, 'r')

nums = [] # empty list

for line in fhand:
    match = re.search("New\\sRevision:\\s(\\d+)", line) # \\s for space and \\d+ for digits to search through in case it is not always 5 numbers
    if match: # double \ since it won't register a backslash without 2 \\
        nums.append(int(match.group(1))) # to print just group 1

print(sum(nums)/len(nums)) # calculate the average

# check EX2Findall to see the code with Findall used. (can't use sum with findall)
