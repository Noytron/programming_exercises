Exercise 0 – Python for everybody chapter 1 knowledge
# What the Python interpreter is and how it differs from a compiler?
# Interpretor is one line 
# Compiler compiles multiline 

# What is the difference between syntax errors, logic errors and semantic errors?
# Syntax errors are grammar errors. Where your code does not follow the standard of coding, 
#when its missing a sign or similar.
# Logic errors are when the program works, but it does the wrong thing.
# Semantic errors relate to the meaning and context.

# What is a program?
# A program is software on a computer that does what it is instructed to do in the code.

# What is input and output?
# Input is what you put in either via keyboard or sensors
# Output is what the program does or prints out

# What is sequential execution?
# Means that the program starts from line 1 and executes it, then continues to line 2 and so on.

# Which 4 things should you do when debugging?
# Reading, Running, Ruminating and retreating

Exercise 1 - Python for everybody chapter 2 knowledge - part 1
# What types of values is Python using?
# str, int, float. complex? 

# How would you check a values type in a Python program?
# type(obj)

# What are variables?
# test = "" <- That is a variable

# What are reserved words?
# Reserved words are words python use, so you cant use them as a variable, fx 'is' or 'class'


# What is a statement?
# x=2 is and assignment statement
# print(1) is an expression statement

Exercise 2 - Python for everybody chapter 2 knowledge - part 2
# What is the purpose of mnemonic naming?
# You name your variables smart so you can remember them later and easily read the code again

# Give at least 3 examples of mnemonic naming.
# patients, animals, days

# Name pythons 6 operators and their syntax
# + - * / // %

# What is order of operations?
# Like Math, */ before +-

# What is concatenation?
# when you add two strings

# How do you ask the user for input in a Python program?
# input("")

# How do you insert comments in a Python program?
# By putting # first in the line


Python for everybody

Exercise 3

Chapter 2

#Exercise 1: Type the following statements in the Python interpreter to see what they do:

5

X = 5

X + 1

6

#Exercise 2: Write a program that uses input to prompt a user for their name and then welcomes them.

>>> name = input('Enter your name:\n')
Enter your name:
Jacob

>>> print(name)
Jacob

>>> print('Hello and welcome' name)
  File "<stdin>", line 1
    print('Hello and welcome' name)
                              ^
SyntaxError: invalid syntax

>>> print('Hello and welcome'), name)
  File "<stdin>", line 1
    print('Hello and welcome'), name)
                                    ^
SyntaxError: unmatched ')'

>>> print('Hello', name)
Hello Jacob

>>> print('Hello_and_welcome', name)
Hello_and_welcome Jacob

>>> print('Hello','and','welcome', name)
Hello and welcome Jacob

>>> print('Hello', name , 'and', 'welcome')
Hello Jacob and welcome


#Exercise 3: Write a program to prompt the user for hours and rate per hour to compute gross pay.

Enter hours: 35

Enter Rate: 2.75

Pay: 96.25

>>> h = 35

>>> r = 2.75

>>> p = h * r

>>> print(p)
96.25

>>> hours = float(input('Enter hours:'))
Enter hours:35

>>> rate = float(input('Enter rate:'))
Enter rate:2.75

>>> pay = hours * rate

>>> print(pay)
96.25

#Exercise 4: Assume that we execute the following assignment statements:

Width = 17

Height = 12.0


>>> width = 17

>>> height = 12.0

>>> width//2

8

>>> width/2.0

8.5

>>> height/3

4.0

>>> 1+2*5

11

#Exercise 5: Write a program which prompts the user for a Celsius temperature,
#convert the temperature to Fahrenheit, and print out the converted temperature.

>>> celsius = 15

>>> fahrenheit = celsius*9/5+32

>>> print(fahrenheit)
59.0

>>> celsius = float(input('Celsius:'))

Celsius:18

>>> fehrenheit = float(celsius*9/5+32)

>>> print(fahrenheit, 'temperature in Fahrenheit')

59.0 temperature in Fahrenheit

>>> print('temperature in Fahrenheit',fahrenheit)

temperature in Fahrenheit 59.0
