#1.14 Exercises

#Ex 1: What is the function of the secondary memory in a computer?
#a) Execute all of the computation and logic of the program
#b) Retrieve web pages over the internet
#c) Store information for the long term, even beyond a power cycle
#d) Take input from the user

#The function is to store information even if the power goes out so C is correct.

#Ex 2: What is a program
#A program is a sequence of statements which will solve the particular problem you programmed it to solve if we assume the script does not have mistakes in it.

#Ex 3: What is the difference between a compiler and an interpreter
#An interpreter can be handled more interactively where as a compiler requires an entire program in a file in order to make it readable.

#Ex 4: Which of the following contains "machine code"?
#a) The Python interpreter
#b) The keyboard
#c) python source file
#d) A word processing document

#The python interpreter  compiled into machine code. so a)

#Ex 5: What is wrong with the following code:
>>> primt 'Hello world!'
File "<stdin>", line 1
primt 'Hello world!'

SyntaxError: invalid syntax
>>>

#Syntax errors i was told is usually typos / grammar of sorts. In this case it would be primt instead of print. However if we assume the person did spell print right but forgot an apostrophe then the missing apostrophe could also have been a syntax error. 

#there also exists Logic errors and Semantic errors.

#Ex 6: Where in the computer is a variable such as "x" stored after the following python line finishes
#x = 123

#a) Central Processing Unit
#b) Main Memory
#c) Secondary Memory
#d) Input Devices
#e) Output Devices

#It is b) Main memory. because it is information that is only present for the duration you have the cpu on. turn it off and the information goes away and you would have to put in the command again later.

#Ex 7: What will the following program print out:
x = 43
x = x + 1
print(x)

#The program will print out 44 because x = 43 becomes a saved variable so the program knows x is 43. So x + 1 means 43 + 1

#Ex 8: Explain each of the following using an example of a human capability:
#(1) Central processing unit,
#(2) Main Memory,
#(3) Secondary Memory,
#(4) Input Device
#(5) Output Device. For example, "What is the human equivalent to a
#Central Processing Unit (CPU)"?

#CPU = brain

#Main memory = things you think about right now. so reading you think about each word but
#once you focus on something else you leave the info behind.

#Secondary memory = long term things like your birthday. You won't forget your birthday
#after sleeping for 8 hours.

#Input & Output = voice, legs, arms, hands, knees etc basically our body and the things we can do with them.

#Ex 9: How do you fix a "Syntax Error"? 
#look through the code and find the grammar mistake you made. or start over which i do not
#recommend unless it is a miniscule project.
