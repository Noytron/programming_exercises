# Exercise 5: Take the following Python code that stores a string:
# str = 'X-DSPAM-Confidence:0.8475'
# Use find and string slicing to extract the portion of the string
# after the colon character and then use the float function to convert
# the extracted string into a floating point number.

str = "X-DSPAM-Confidence:0.8475"
# first i want to find the index of the colon ":" so i type
# print(str.find(":")) 
# and run the program and to find that the ":" is 18

col_index = str.find(":")
# print(str[col_index+1:]) run this to see the specified col_index+1
# Which checks out with the 0.8475 that is behind the ":"
num = float(str[col_index+1:]) 
# I slice the string (str[col_index+1:]) 
# I leave it empty after : because I want everything behind the ":"
# By using Float I convert the function into what i want.
print(num)
print(type(num))
# By running the print of these i confirm it extracts what i want.

