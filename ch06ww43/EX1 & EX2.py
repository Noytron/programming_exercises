# Exercise 1: Write a while loop that starts at the last character 
# in the string and works its way backwards to the first character
# in the string, printing each letter on a separate line, except backwards.

fruit = 'banana'

index = 0
while index > -len(fruit):
    index = index - 1  
    letter = fruit[index] 
    print(letter) 

# I swap index = index+1 with letter = fruit[index]
# And place print(letter) under letter = fruit[index]
# I also swap < so index should be bigger than -len instead of < len(fruit)
# And i changed index = index +1 to be -1.

# book example:
# fruit = 'banana'
# len(fruit)

# last = fruit[-1]
# print(last)

# index = 0
# while index < len(fruit):
  #  letter = fruit[index]
   # print(letter)
    #index = index +1

# Exercise 2: Given that fruit is a string, what does fruit[:] mean?
# fruit is still a string and the [:] allows you to work with the string.

