#3.11 Exercises

Ex 1: Rewrite your pay computation to give the employee 1.5 times the hourly rate
for hours worked above 40 hours. 

Enter hours: 45
Enter Rate: 10
Pay: 475.0

#h = hours
>>> h = float(input('Enter work hours:'))
Enter work hours: 45

#r = rate 
>>> r = float(input('Enter rate per hour:'))
Enter rate per hour: 10

#np = normal pay 
>>> np = (h - (h - 40)) * r 

#e = extra
>>> e = (h - 40) * (r * 1.5)

#p = pay 
>>> p = np + e 


>>> h = float(input('Enter work hours:'))
45
>>> r = float(input('Enter rate per hour:'))
10

if h < 40:
    np = h*r 
else:
        np = (h - (h - 40)) * r 
        e = (h - 40) * (r * 1.5)
        p = np + e 
>>> print('Hours worked: ' + str(int(h)) + ' Payment for hours worked: ' + str(int(p)))

#Remember to tab inside 'if' and 'else' ****

Ex 2: Rewrite your pay program using try and except so that your program handles
non-numeric input gracefully by printing a message and exiting the program.

Enter Hours: 20
Enter Rate: nine
Error, please enter numeric input 

Enter Hours: forty
Error, please enter numeric input 

>>> try:
        h = float(input('Enter hours:'))
        r = float(input('Enter rate:'))
    except:
        print('please enter a number')

Enter hours: 20
Enter rate: nine 
please enter a number 

Enter hours: forty 
Enter rate: 9
please enter a number

Ex 3: Write a program to prompt for a score between 0.0 and 1.0 If the score
is out of range, print an error message. If the score is between 0.0 and 1.0,
print a grade using the following table: 

score   grade
>= 0.9  A 
>= 0.8  B 
>= 0.7  C
>= 0.6  D 
<  0.6  F 

Enter score: 0.95
A 

Enter score: perfect
bad score 

enter score: 10
bad score 

enter score: 0.75
C

enter score: 0.5
F 

try:
    score=float(input('enter score between 0.0 and 1.0:'))
    if(score>1.0 or score <0.0):
        print('bad score')
    elif(score>=0.9):
        print('A')
    elif(score>=0.8):
        print('B')
    elif(score>=0.7):
        print('C')
    elif(score>=0.6):
        print('D')
    elif(score<0.6):
        print('F')
except:
    print('bad score')
