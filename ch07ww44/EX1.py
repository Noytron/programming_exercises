# Exercise 1: Write a program to read through a file and print
# the contents of the file (line by line) in all upper case.
# Executing the program will look as follows:

# python shout.py
# enter a file name: mbox-short.txt
# FROM STEPHEN.MARQUARD@UCT.AC.ZA SAT JAN 5 09:14:16 2008
# RETURN-PATH: <POSTMASTER@COLLAB.SAKAIPROJECT.ORG>
# RECEIVED: FROM MURDER (MAIL.UMICH.EDU [141.211.14.90])
#   BY FRANKENSTEIN.MAIL.UMICH.EDU (CYRUS V2.3.8) WITH LMTPA;
#   SAT, 05 JAN 2008 09:14:16 -0500

fhand = open('mbox-short.txt', 'r') #  What to open and r to read
for line in fhand:
    print(line.upper()) # .upper() to make it all uppercase
# in my case this brings up the terminal because it has trouble finding it 
# despite being in the exact folder it has to be in.
# So once in the terminal i backtrack the path to the folder
# Which in my case is:
# C:\Users\jacob\OneDrive\Skrivebord\IT-Technology\programming\EX\ww44
# once i have written that again and run the program while the terminal is up it works.
    