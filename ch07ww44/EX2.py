# Exercise 2: Write a program to prompt for a file name,
# and then read through the file and look for lines of the form:

#  X-DSPAM-Confidence: 0.8475

# When you encounter a line that starts with "X-DSPAM-Confidence:" 
# pull apart the line to extract the floating-point number on the line.
# Count these lines and then compute the total of the spam confidence
# values from these lines. When you reach the end of the file,
# print out the average spam confidence.

# Enter the file name: mbox.txt
# Average spam confidence: 0.894128046745

# Enter the file name: mbox-short.txt
# Average spam confidence: 0.750718518519

# Test your file on the mbox.txt and mbox-short.txt files.

mboxfiles = input("Enter the file name: ")
fhand = open(mboxfiles, 'r')

count = 0
total_confidence = 0
for line in fhand:
    if line.startswith("X-DSPAM-Confidence:"): 
        # To search for lines starting with X-DSPAM-Confidence: 
        space_index = line.find(" ")
        my_num = float(line[space_index+1:])
        print(my_num)
        # line 28-30 will only execute if it finds a line with X-DSPAM-Confidence:
        total_confidence += my_num
        count += 1 
        # line 32-33: To get the sum and the count of all the spam confidences

print("Average spam confidence: ", total_confidence/count)
# My average for mbox.txt becomes 0.8914 instead of 0.8941
# so it is possible i didn't download exactly everything or
# some numbers were lost. But overall it works and got somewhat close.
# Average spam confidence:  0.8914086672879774

# My Average for mbox-short.txt was not off luckily enough.
# Average spam confidence:  0.7507185185185187

# Note i have to cd into each part towards ww44 and leave the terminal
# open so i can run the program.