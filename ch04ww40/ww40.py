Python Chapter 4

EX 1: Run the program on your system and see what numbers you get.
Run the program more than once and see what numbers you get.

for i in range(10):
    x = random.random()
    print(x)
 #1 random roll   
0.03456215796314299
0.46232163436236473
0.310216713372083
0.7337989613694662
0.5698465632056274
0.6552353414948895
0.7916107358931123
0.27630680200597457
0.010315757449643637
0.7831368881708612

#2 random roll 
0.004456058939049101
0.9529360353201767
0.9072888895803481
0.9859797434874047
0.48225183376299385
0.6247553774528102
0.309320931827448
0.7157300965648057
0.08750090240788766
0.46429680182953037

EX 2: Move the last line of this program to the top, so the function call appears before
the definitions. Run the program and see what error message you get.


def print_lyrics():
    print("I'm a lumberjack, and I'm okay.")
    print('I sleep all night and I work all day.')
repeat_lyrics()

def repeat_lyrics():
    print_lyrics()
    print_lyrics()

#the message just becomes:
#NameError: name 'repeat_lyrics' is not defined. 

EX 3: Move the function call back to the bottom and move the definition of
print_lyrics after the definition of repeat_lyrics. What happens when you run this program?

def repeat_lyrics():
    print_lyrics()
    print_lyrics()

def print_lyrics():
    print("I'm a lumberjack, and I'm okay.")
    print('I sleep all night and I work all day.')
    
repeat_lyrics()

#It executes as normal

EX 4: What is the purpose of the "def" keyword in python?
a) It is slang that means “the following code is really cool”
b) It indicates the start of a function
c) It indicates that the following indented section of code is to be stored for later
d) b and c are both true
e) None of the above

# The answer is D because def defines a function and functions are bits of code that 
# you can use again later to save space and time.

EX 5: What will the following Python program print out?

def fred():
    print("Zap")
def jane():
    print("ABC")

jane()
fred()
jane()

a) Zap ABC jane fred jane
b) Zap ABC Zap
c) ABC Zap jane
d) ABC Zap ABC ~~
e) Zap Zap Zap

# It will print ABC Zap ABC so the answer is d) 

EX 6: Rewrite your pay computation with time-and-a-half for over-time
and create a function called computepay which takes two parameters (hours and rate).

Enter hours: 45
Enter Rate: 10
Pay: 475.0

hours = float(input("Enter Hours:"))
#Enter Hours:45
rate = float(input("Enter Rate:"))
#Enter Rate:10
extra=1.5
def computepay(hours, rate):
    if(hours<=40):
        pay=hours*rate
    if(hours>40):
        pay=40*rate+((hours-40)*rate*extra)
    return pay                         
        pay=computepay(hours, rate)  
print(pay)

#return pay needed to actually get a result instead of none
#pay = computepay(hours, rate) this line is required,
# since now 'pay is a part of computepay 


EX 7: Rewrite the grade program from the previous chapter using a function
called computegrade that takes a score as its parameter and returns
a grade as a string.

def computegrade():
    try:
        score=float(input('enter score between 0.0 and 1.0:'))
        if(score>1.0 or score <0.0):
            print('bad score')
        elif(score>=0.9):
            print('A')
        elif(score>=0.8):
            print('B')
        elif(score>=0.7):
            print('C')
        elif(score>=0.6):
            print('D')
        elif(score<0.6):
            print('F')
    except:
        print('bad score')
        
    return 'final score'
        print(computegrade())

# i messed with return a bit but never found a solution that worked. 